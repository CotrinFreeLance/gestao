export class HolderData {
    fullName = '';
    birthDate = '';
    gender = '';
    taxDocument = '';
    phone = '';
    mobile = '';
    company = new Company();
    contact = new Contact();
}

export class Company {
    tradeName = '';
    fullName = '';
    taxDocument = '';
    businessTypeId = '';
}

export class Contact {
    address = '';
    mobile = '';
    phone = '';
    phoneExtension = null;
    email = '';
    site = '';
}

export class Address {
    cityCode = '';
    district = '';
    line1 = '';
    line2 = '';
    streetNumber = '';
    zipCode = '';
}

export class BankAccount {
    isJuristic = '';
    bankNumber = '';
    accountNumber = '';
    bankBranchNumber = '';
    variation = null;
    type = 'corrente';
}

export class User {
    email = '';
    password = '';
}

export class TransferPlan {
    days = '';
    anticipated = false;
    daysOnlineSplit = '';
}

export class Holder {
    holder = new HolderData();
    address = new Address();
    bankAccount = new BankAccount();
    user = new User();
    businessActivityId = '';
    marketingMediaId = '';
    transferPlan = new TransferPlan();
    commercialName = '';
    appUrl = '';
}

export class HolderParams extends Holder {
    constructor(holder) {
        this.holder = {
            fullName: holder.fullName || '',
            birthDate = holder.birthDate || '',
            gender = holder.gender || '',
            taxDocument = holder.taxDocument || '',
            phone = holder.phone || '',
            mobile = holder.mobile || '',
            company = {
                tradeName = holder.Company.tradeName || '',
                fullName = holder.Company.fullName || '',
                taxDocument = holder.Company.taxDocument || '',
                businessTypeId = holder.Company.businessTypeId || ''
            },
            contact = {
                address = holder.Contact.address || '',
                mobile = holder.Contact.mobile || '',
                phone = holder.Contact.phone || '',
                phoneExtension = holder.Contact.phoneExtension || null,
                email = holder.Contact.email || '',
                site = holder.Contact.site || ''
            }
        }
        this.address = {
            cityCode = holder.cityCode || '',
            district = holder.district || '',
            line1 = holder.line1 || '',
            line2 = holder.line2 || '',
            streetNumber = holder.streetNumber || '',
            zipCode = holder.zipCode || ''
        }
        this.bankAccount = {
            isJuristic = holder.isJuristic || '',
            bankNumber = holder.bankNumber || '',
            accountNumber = holder.accountNumber || '',
            bankBranchNumber = holder.bankBranchNumber || '',
            variation = holder.variation || null,
            type = holder.type || '',
        }
        this.user = {
            email = holder.email || '',
            password = holder.password || ''
        }
        this.businessActivityId = holder.businessActivityId || '';
        this.marketingMediaId = holder.marketingMediaId || '';
        this.transferPlan = {
            days = holder.days || '',
            anticipated = holder.anticipated || false,
            daysOnlineSplit = holder.daysOnlineSplit || ''
        }
        this.commercialName = holder.commercialName || '';
        this.appUrl = holder.appUrl || '';
    }
}