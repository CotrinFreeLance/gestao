var error = {

  CITY_NOT_FOUND: 'Cidade não encontrada.',
  BUSINESS_ACTIVITY_NOT_FOUND: 'Ramo de atividade não encontrada.',
  BUSINESS_TYPE_NOT_FOUND: 'Tipo de empresa não encontrado.',
  BANK_NOT_FOUND: 'Banco não encontrado.',
  EMAIL_ALREADY_TAKEN: 'Esse e-mail já está em uso.',
  DUPLICATE_TAX_DOCUMENT: 'O cpf/cnpj está uso.',
  BANK_ACCOUNT_IS_NOT_VALID: 'Dados bancários inválidos.',
  BANK_BRANCH_DIGIT_IS_NOT_VALID: 'Digito da agência inválido.',
  ACCOUNT_DIGIT_IS_NOT_VALID: 'Digito da conta inválido.',
  PAYMENT_ACCOUNT_NOT_CREATED: 'Conta de pagamento não cadastrada',
  APPLICATION_TRANSFER_PLAN_INVALID: 'Plano de transferência não permitido para esse aplicativo.',
  TRANSFER_PLAN_ONLINE_INVALID: 'Plano de transferência online inválido para esse aplicativo.',
  SPLIT_TRANSFER_PLAN_INVALID: 'Plano de transferência do split inválido para esse aplicativo.',
  UNKNOWN_APPLICATION: 'O ID do aplicativo não está associado ao token',
  APPLICATION_NOT_FOUND: 'Aplicativo não encontrado.',
  HOLDERS_NOT_FOUND: 'Titular não encontrado.',
  AMOUNT_MINOR_FEES_BASE: 'Valor inferior da taxa base',
  PARTNER_BANK_SLIP_FEES_NOT_FOUND: 'O parceiro não possui taxa cadastrada.',
  HOLDER_BANK_SLIP_FEES_NOT_FOUND: 'O lojista não possui taxa cadastrada.'

}

export default error
