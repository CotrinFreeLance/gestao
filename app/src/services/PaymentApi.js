import axios from 'axios'
import errors from './errors'

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IlVzZXIiLCJBcHBsaWNhdGlvbklkIjoiMTQwIiwiSG9sZGVySWQiOiI1MDciLCJVc2VySWQiOiI1MDciLCJMaW5rQ29kZSI6IjI1ODIxNjk2IiwiU2FsdCI6ImI4ZjJkMzM3MzE3MjQwZmE5ZjY1ZTI3Y2MxMWUzNThiIiwiaXNzIjoiYTA3MzhjZGQiLCJhdWQiOiJlYTJiNDdlYWJjMDQzIn0.SIQI4O8c_QoZxrBYyYoOOWnQyXm0gXg__lPAO4t7u1w'

const base = {
  headers: {
    Authorization: `Bearer ${token}`
  }
}

const instance = axios.create(base)

class BankSlipFee {
  async create (model) {
    try {
      const build = await instance.post(`${process.env.VUE_APP_PAYMENTS_API_URL}/partner/fees/bank-slips`, {
        holders: model
      })

      return build
    } catch (server) {
      if (server.response.status === 422) {
        server.response.data.error = errors[server.response.data.error]
      }

      throw server.response.data
    }
  }

  async update (model) {
    try {
      const build = await instance.put(`${process.env.VUE_APP_PAYMENTS_API_URL}/partner/fees/bank-slips`, {
        holders: model
      })
      return build
    } catch (server) {
      if (server.response.data.error !== 'HOLDER_BANK_SLIP_FEES_NOT_FOUND') {
        server.response.data.nonext = true
      }

      if (server.response.status === 422) {
        server.response.data.error = errors[server.response.data.error]
      }

      throw server.response.data
    }
  }

  async getBaseFee () {
    const found = await instance.get(`${process.env.VUE_APP_PAYMENTS_API_URL}/partner/fees/plan-fees`)

    var bankSlipFee = found.bankSlipsFee

    return bankSlipFee
  }
}

export class PaymentApi {
  bankSlipFee = new BankSlipFee();
}
