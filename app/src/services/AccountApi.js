import axios from 'axios'
import errors from './errors'

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA5LzA5L2lkZW50aXR5L2NsYWltcy9hY3RvciI6IlVzZXIiLCJBcHBsaWNhdGlvbklkIjoiMTQwIiwiSG9sZGVySWQiOiI1MDciLCJVc2VySWQiOiI1MDciLCJMaW5rQ29kZSI6IjI1ODIxNjk2IiwiU2FsdCI6ImI4ZjJkMzM3MzE3MjQwZmE5ZjY1ZTI3Y2MxMWUzNThiIiwiaXNzIjoiYTA3MzhjZGQiLCJhdWQiOiJlYTJiNDdlYWJjMDQzIn0.SIQI4O8c_QoZxrBYyYoOOWnQyXm0gXg__lPAO4t7u1w'

const base = {
  headers: {
    Authorization: `Bearer ${token}`
  }
}

const instance = axios.create(base)

class HolderBuilder {
  async create (model) {
    try {
      const build = await instance.post(`${process.env.VUE_APP_ACCOUNT_API_URL}/${process.env.VUE_APP_APPLICATION_ID}/signup/seller`, model)

      return build
    } catch (server) {
      if (server.response.status === 422) {
        server.response.data.error = errors[server.response.data.error]
      }

      throw server.response.data
    }
  }

  async getAll () {
    const allHolders = await instance.get(`${process.env.VUE_APP_ACCOUNT_API_URL}/partner/sellers`)

    return allHolders
  }

  async get (params) {
    const model = {
      fullNames: [params.fullName],
      taxDocuments: [params.taxDocument]
    }

    const found = await instance.post(`${process.env.VUE_APP_ACCOUNT_API_URL}/partner/sellers`, model)

    return found
  }

  async getCityCode (params) {
    var instance = axios.create()
    delete instance.defaults.headers.common.Authorization

    const found = await axios.get(`${process.env.VUE_APP_CITY_CODE_API_URL}/${params}/json`)

    return found
  }

  async getBusinessActivities () {
    const found = await instance.get(`${process.env.VUE_APP_ACCOUNT_API_URL}/business-activities`)

    return found
  }

  async getBusinessTypes () {
    const found = await instance.get(`${process.env.VUE_APP_ACCOUNT_API_URL}/business-types`)

    return found
  }

  async getBanks () {
    const found = await instance.get(`${process.env.VUE_APP_ACCOUNT_API_URL}/banks`)

    return found
  }
}

export class Api {
  holders = new HolderBuilder();
}
