
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/register', component: () => import('pages/registers/Main.vue'), name: 'register' },
      { path: '/details/:id', component: () => import('pages/details/MainDetail.vue'), name: 'details' },
      { path: '/help', component: () => import('pages/Help.vue'), name: 'help' },
      { path: '/search', component: () => import('pages/Search.vue'), name: 'search' },
      { path: '', redirect: 'search' }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
